//
//  ViewController.m
//  Tetris
//
//  Created by meer07 on 2014/04/18.
//
//  Objective-C版 テトリス
//  左右スワイプ:ブロックの左右移動 下スワイプ:落下スピード増加 タップ:開発


#import "ViewController.h"
#import "Status.h"
#import "Block.h"

@interface ViewController (){
    NSMutableArray *field; // フィールド情報の格納
    UIImageView *wall,*block; // 壁とブロック部品のImageView
    NSTimer *timer;
    int width,hight,endcount,score;
    Status *status;
    Block *blockdata;
    UISwipeGestureRecognizer *left,*right,*under;
    UITapGestureRecognizer *tap;
}

@end

@implementation ViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    endcount = 3;
    score = 0;
    // BlockクラスとStatusクラスを初期化
    status = [Status new];
    blockdata = [Block new];
    [blockdata blockinit];
    
    // 状態の初期化
    [self InitStaus];
    
    // フィールドを作成
    [self fieldSet];
    
    timer = [NSTimer scheduledTimerWithTimeInterval:0.5f target:self selector:@selector(Update:) userInfo:nil repeats:YES];
    
    [self SwipandTapInit];
}

// フィールドをつくる
-(void)fieldSet{
    width = 12;
    hight = 18;
    
    field = [NSMutableArray arrayWithCapacity:width];
    for (int h = 0; h < hight; h++) {
        if (h == hight-1) {
            // 地面の情報を格納
            [field addObject:[NSMutableArray arrayWithObjects:@1,@1,@1,@1,@1,@1,@1,@1,@1,@1,@1,@1,nil]];
        }else{
            [field addObject:[NSMutableArray arrayWithObjects:@1,@0,@0,@0,@0,@0,@0,@0,@0,@0,@0,@1,nil]];
        }
    }

    for (int a = 0; a < hight; a++) {
        for (int b = 0; b < width; b++) {
            if ([(NSNumber*)field[a][b] intValue] > 0) {
                // 壁の場合
                if (b == 0 || a == hight - 1 || b == width - 1) {
                    [self SetWall:b setY:a];
                }
            }
        }
    }

}

// 1フレーム毎に実行する関数(更新処理)
-(void)Update:(NSTimer *)timer{
    status.y += 0.5;
    
    // 落ちるとフィールドと被る(衝突状態)ときブロックはその場にとどまる。
    if ([self check:status.x checkY:status.y+1] == true) {
        status.y = floorf(status.y);
    }
    
    if ([self check:status.x checkY:status.y+1] == true) {
        if (--endcount <= 0) {
            // 固定処理
            [self EndProcess];
        }else{
            endcount = 1;
        }
    }
    
    
    [self Draw];
}

// 状態の初期化
-(void)InitStaus{
    int ramval = arc4random()%6;
    status.x = 4;
    status.y = 0;
    status.block = blockdata.array[ramval];
}

// フィールドを描画
-(void)SetWall:(int)x setY:(int)y{
    wall = [UIImageView new];
    wall.image = [UIImage imageNamed:@"WallBlock.png"];
    wall.frame = CGRectMake(x*wall.image.size.width, y*wall.image.size.height, wall.image.size.width, wall.image.size.height);
    [self.view addSubview:wall];
}

// ブロックを描画
-(void)SetBlock:(int)x setY:(int)y{
    block = [UIImageView new];
    block.image = [UIImage imageNamed:@"Block.png"];
    block.frame = CGRectMake(x*block.image.size.width, y*block.image.size.height, block.image.size.width, block.image.size.height);
    [self.view addSubview:block];
}

-(void)Draw{
    // 描画されているものを一旦全削除
    for (UIView *view in [self.view subviews]) {
        [view removeFromSuperview];
    }
    
    
    // 固定されたブロックを描画,または壁を再描画
    for (int a = 0; a < hight; a++) {
        for (int b = 0; b < width; b++) {
            if ([(NSNumber*)field[a][b] intValue] > 0) {
                // 壁の場合
                if (b == 0 || a == hight - 1 || b == width - 1) {
                    [self SetWall:b setY:a];
                    // ブロックの場合
                }else{
                    [self SetBlock:b setY:a];
                }
            }
        }
    }
    
    
    int blocksize = blockdata.blocksize;
    // 落ちているときのブロックを描画
    for (int y = 0; y < blocksize; y++) {
        for (int x = 0; x < blocksize; x++) {
            int obv = [(NSNumber*)status.block[y][x] intValue];
            if (obv > 0) {
                //NSLog(@"%d",obv);
                [self SetBlock:(status.x+x) setY:(status.y+y)];
            }
        }
    }

    [self ScoreView];
}

// 衝突判定
-(BOOL)check:(int)x checkY:(int)y{
    NSArray *array = status.block;
    
    for (int a = 0; a < blockdata.blocksize; a++) {
        for (int b = 0; b < blockdata.blocksize; b++) {
            // ブロックがあってその下にフィールドの壁があるとき
            if ([(NSNumber*)array[a][b] intValue] > 0 && [(NSNumber*)field[a+y][b+x] intValue] > 0) {
                return true;
            }
        }
    }
    return false;
}

// ブロックが着底したとき
-(void)EndProcess{
    [timer invalidate];
    [self Lock]; // 固定
    [self DeleteLine]; // 削除
    [self Draw]; // 再描画
    
    [self InitStaus]; // ブロック情報の初期化
    endcount = 3;
    
    // タイマーを再開する
    timer = [NSTimer scheduledTimerWithTimeInterval:0.5f target:self selector:@selector(Update:) userInfo:nil repeats:YES];
}

// ブロックの固定処理
-(void)Lock{
    for (int y = 0; y < blockdata.blocksize; y++) {
        for (int x = 0; x < blockdata.blocksize; x++) {
            if ([(NSNumber*)status.block[y][x] intValue] > 0) {
                int a = status.y + y;
                int b = status.x + x;
                // フィールドにブロックの情報を入れる
                field[a][b] = status.block[y][x];
            }
        }
    }

}

// ライン消去
-(void)DeleteLine{
    for (int i = hight - 2; i >= 0; i--) {
        bool deleteflag = true;
        for (int j = 1; j <= width - 2; j++) {
            if ([(NSNumber *)field[i][j] intValue] == 0) {
                deleteflag = false;
                break;
            }
        }
        
        // ラインを消去するフラグが立っている場合
        if (deleteflag == true) {
            score += 100;
            for (int y = i - 1; y >= 0; y--) {
                for (int x = 1; x <= width -2; x++) {
                    int yy = y+1;
                    field[yy][x] = field[y][x];
                }
            }
            // iをインクリメントしてもう１度同じ列を走査する
            i++;
        }
    }
}

// ジェスチャーの登録
-(void)SwipandTapInit{
    left = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(LeftSwip:)];
    right = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(RightSwip:)];
    under = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(UnderSwip:)];
    tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(TapGes:)];
    
    left.direction = UISwipeGestureRecognizerDirectionLeft;
    right.direction = UISwipeGestureRecognizerDirectionRight;
    under.direction = UISwipeGestureRecognizerDirectionDown;
    
    [self.view addGestureRecognizer:left];
    [self.view addGestureRecognizer:right];
    [self.view addGestureRecognizer:under];
    [self.view addGestureRecognizer:tap];
}

// 左にスワイプしたとき
-(void)LeftSwip:(UISwipeGestureRecognizer *)gesture{
    int ceilY = ceilf(status.y);
    int floorY = floorf(status.x);
    
    if(![self check:status.x - 1 checkY:ceilY] && ![self check:status.x - 1 checkY:floorY]){
        status.x--;
    }
}

// 右にスワイプしたとき
-(void)RightSwip:(UISwipeGestureRecognizer *)gesture{
    int ceilY = ceilf(status.y);
    int floorY = floorf(status.x);
    
    if(![self check:status.x + 1 checkY:ceilY] && ![self check:status.x + 1 checkY:floorY]){
        status.x++;
    }

}

// 下にスワイプしたとき
-(void)UnderSwip:(UISwipeGestureRecognizer *)gesture{
    status.y += 1.5;
    
    // 地面に衝突したとき
    if ([self check:status.x checkY:status.y+1]) {
        status.y -= 1.5;
    }
}

// タップしたとき(回転処理)
-(void)TapGes:(UITapGestureRecognizer *)gesture{
    // 回転させるときは必ずコピーしましょう!!!!!!
    // 注意!! 通常の配列コピーは編集不可(値の上書き等...)
    NSMutableArray *copyarray = [NSKeyedUnarchiver unarchiveObjectWithData:[NSKeyedArchiver archivedDataWithRootObject:status.block]];
    //NSLog(@"%@",copyarray);
    for (int y = 0; y < blockdata.blocksize; y++) {
        for (int x = 0; x < blockdata.blocksize; x++) {
            copyarray[x][blockdata.blocksize-1-y] = status.block[y][x];
            //NSLog(@"%@",copyarray);
        }
    }
    
    status.block = [NSKeyedUnarchiver unarchiveObjectWithData:[NSKeyedArchiver archivedDataWithRootObject:copyarray]];
    //NSLog(@"%@",status.block);
    
    // 回転したときに衝突してしまったら
    if ([self check:status.x checkY:status.y]) {
        for (int y = 0; y < blockdata.blocksize; y++) {
            for (int x = 0; x < blockdata.blocksize; x++) {
                // 元に戻す
                copyarray[blockdata.blocksize-1-x][y] = status.block[y][x];
                //NSLog(@"%@",copyarray);
            }
        }
        // 再び代入
        status.block = [NSKeyedUnarchiver unarchiveObjectWithData:[NSKeyedArchiver archivedDataWithRootObject:copyarray]];
    }
}

// スコアの設定
-(void)ScoreView{
    UILabel *label = [UILabel new];
    label.frame = CGRectMake(self.view.center.x/2, 0, 100, 30);
    label.text = [NSString stringWithFormat:@"%d", score];
    [self.view addSubview:label];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end