//
//  Block.h
//  Tetris
//
//  Created by 海下直哉 on 2014/04/20.
//  Copyright (c) 2014年 meer07. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Block : NSObject

@property (nonatomic) NSArray *array;
@property (nonatomic) int blocksize;
-(void)blockinit;
@end
